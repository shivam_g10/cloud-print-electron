// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.
const ipc = require('electron').ipcRenderer;
const ENV = require('./env.js');
const TEST_URL = ENV.SOCKET_URL;

let selectedPrinter = null;

function sendIPC(html, deviceName) {
    return ipc.send("printPage", {
        html,
        deviceName
    });
}

function getListOfPrinters() {
    ipc.send('get-printer-list');
}

function processPrinterList(event, printers) {
    const printerHtmlStr = `
        <option selected disabled hidden>Choose printer</option>
        ${printers.map((printer) =>
    {
        return `
                <option value='${printer.name}'>${printer.displayName}</option>
            `
    }).join('')}
    `;

    document.querySelector('#printer-list select').innerHTML = printerHtmlStr;

    getSelectedPrinter();
}

function processSelectedPrinter(event, deviceName) {
    selectedPrinter = deviceName;
    if (deviceName == 404) {
        alert('Printer not found');
        return;
    } else if (deviceName) {
        document.querySelector('#selected-printer-name').textContent = selectedPrinter;
        document.querySelector('#printer-list select option[selected]').removeAttribute('selected');
        document.querySelector(`#printer-list select option[value="${deviceName}"]`).setAttribute('selected', '');
    }
}

function getSelectedPrinter() {
    ipc.send('get-selected-printer');
}

function selectPrinter() {
    deviceName = document.querySelector('#printer-list select').value;
    ipc.send('printer-selected', deviceName);
}

function setUpEvents() {
    ipc.on('printer-list', processPrinterList);
    ipc.on('selected-printer', processSelectedPrinter);
    getListOfPrinters();
}

function _print(content) {
    sendIPC(content);
}

function connectionActive() {
    document.querySelector('#connection-status').textContent = 'Active';
    document.querySelector('#reconnect-button').setAttribute('hidden', '');
}

function connectionInactive() {
    document.querySelector('#connection-status').textContent = 'Inactive';
    document.querySelector('#reconnect-button').removeAttribute('hidden');
}

window.addEventListener('DOMContentLoaded', () => {
    setUpEvents();
    connect(TEST_URL);
});


const log = require('electron-log');
const WebSocket = require('ws');

log.info("Loading Socket");

function connect(url) {
    const socket = new WebSocket(url);

    socket.on('message', (data) => {
        try {
            const htmlData = JSON.parse(data.toString());
            _print(htmlData.html);
        } catch (err) {
            console.log(data.toString());
        }
    });

    socket.on('error', (e) => {
        log.info(e); // not displayed
    });

    socket.on('ok', () => {
        log.info("OK received renderer"); // not displayed
    });

    log.info(socket);

    socket.on('open', (data) => {
        log.info("connected renderer", data); // displayed
        connectionActive();
    });

    socket.on('close', () => {
        connectionInactive();
    })
}


const elts = {
    text1: document.getElementById("text1"),
    text2: document.getElementById("text2")
};

const texts = [
    "Res2Go",
    "Printer",
    "Middleware",
    ":)",
];

const morphTime = 1;
const cooldownTime = 0.25;

let textIndex = texts.length - 1;
let time = new Date();
let morph = 0;
let cooldown = cooldownTime;

elts.text1.textContent = texts[textIndex % texts.length];
elts.text2.textContent = texts[(textIndex + 1) % texts.length];

function doMorph() {
    morph -= cooldown;
    cooldown = 0;

    let fraction = morph / morphTime;

    if (fraction > 1) {
        cooldown = cooldownTime;
        fraction = 1;
    }

    setMorph(fraction);
}

function setMorph(fraction) {
    elts.text2.style.filter = `blur(${Math.min(8 / fraction - 8, 100)}px)`;
    elts.text2.style.opacity = `${Math.pow(fraction, 0.4) * 100}%`;

    fraction = 1 - fraction;
    elts.text1.style.filter = `blur(${Math.min(8 / fraction - 8, 100)}px)`;
    elts.text1.style.opacity = `${Math.pow(fraction, 0.4) * 100}%`;

    elts.text1.textContent = texts[textIndex % texts.length];
    elts.text2.textContent = texts[(textIndex + 1) % texts.length];
}

function doCooldown() {
    morph = 0;

    elts.text2.style.filter = "";
    elts.text2.style.opacity = "100%";

    elts.text1.style.filter = "";
    elts.text1.style.opacity = "0%";
}

function animate() {
    requestAnimationFrame(animate);

    let newTime = new Date();
    let shouldIncrementIndex = cooldown > 0;
    let dt = (newTime - time) / 1000;
    time = newTime;

    cooldown -= dt;

    if (cooldown <= 0) {
        if (shouldIncrementIndex) {
            textIndex++;
        }

        doMorph();
    } else {
        doCooldown();
    }
}

animate();