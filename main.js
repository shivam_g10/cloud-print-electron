// Modules to control application life and create native browser window
const { app, BrowserWindow, ipcMain } = require('electron');
const path = require('path');
const {readFileSync, createWriteStream, existsSync} = require('fs');

app.commandLine.appendSwitch('--disable-gpu');
app.commandLine.appendSwitch('--disable-software-rasterizer');
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
const STORE_FILE_NAME = 'store.json';

/**
 * 
 * @param {Electron.app} app 
 */
function getFileStore() {
    const filePath = path.join(app.getPath('userData'), STORE_FILE_NAME);
    if(!existsSync(filePath))
    {
        return {};
    }
    const storeData = readFileSync(filePath, {encoding: 'utf8'});

    try
    {
        return JSON.parse(storeData);
    }
    catch(e)
    {
        return {};
    }
};

function saveFileStore(app, store) {
    const filePath = path.join(app.getPath('userData'), STORE_FILE_NAME);
    const ws = createWriteStream(filePath, { encoding: 'utf8' });
    ws.write(JSON.stringify(store), (err) => {
        if(err) console.log(err);
        ws.end();
    });
}

function createWindow()
{
    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true,
            devTools: false, // change to true for development
            icon: './assets/logo-ico.ico'
        }
    });
    
    mainWindow.setMenu(null);  // remove menus

    // and load the index.html of the app.
    mainWindow.loadFile('index.html')


    // Emitted when the window is closed.
    mainWindow.on('closed', function ()
    {
        mainWindow = null
    })

    mainWindow.maximize();
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function ()
{
    if (process.platform !== 'darwin') app.quit();
})

app.on('activate', function ()
{
    if (mainWindow === null) createWindow()
})

ipcMain.on("printPage", (event, { html, deviceName }) =>
{
    const store = getFileStore(app);

    if(store.selectedPrinter)
    {
        deviceName = store.selectedPrinter;
    }

    const printWindow = new BrowserWindow({ show: false });

    printWindow.loadURL("data:text/html," + encodeURI(html));

    printWindow.webContents.on('did-finish-load', () =>
    {
        const options = {
            silent: true,
            printBackground: true,
            color: false,
            marginType: 'none'
        };

        if (deviceName)
        {
            options.deviceName = deviceName;
        }
        printWindow.webContents.print(options);

        setTimeout(() =>
        {
            printWindow.close();
        }, 10 * 1000)

    });

});

ipcMain.on('get-printer-list', (event) =>
{
    mainWindow.webContents.getPrintersAsync().then((data) =>
    {
        event.sender.send('printer-list', data);
    });
});

ipcMain.on('get-selected-printer', (event) =>
{
    const store = getFileStore(app);
    let selectedPrinter = null;
    if(store.selectedPrinter) {
        selectedPrinter = store.selectedPrinter;
    }
    event.sender.send('selected-printer', selectedPrinter)
});

ipcMain.on('printer-selected', (event, deviceName) =>
{
    mainWindow.webContents.getPrintersAsync().then((data) =>
    {
        const printer = data.filter((p) => { return p.name === deviceName })[0];

        if (!printer)
        {
            event.sender.send('selected-printer', 404);
        }
        else
        {
            const store = getFileStore(app);
            store.selectedPrinter = deviceName;
            saveFileStore(app, store);
            event.sender.send('selected-printer', deviceName);
        }
    });
});
